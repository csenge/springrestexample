package restExample.restController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import restExample.controller.UserController;
import restExample.model.User;

@RestController()
public class UserRestController {

    @Autowired
    UserController userController;

    @RequestMapping("/user")
    public User getUser(@RequestParam(value="name") String name) {
        User dummyUser = new User(12, "Not Csenge");
        return dummyUser;
    }

    @GetMapping("/getUser/{id}")
    public User getGreeting() {
        User dummyUser = new User(12, "Csenge");
        return dummyUser;
    }

    @GetMapping("/getAllUsers")
    public List<User> getAllUsers() {
        return userController.getAllUsers();
    }

    @PostMapping("/insertUser")
    public User insertUser(@RequestBody User user) {
        return userController.insertUser(user);
    }

    @PutMapping("/updateUser")
    public User updateUser(@RequestParam long id,
                           @RequestBody User user
//                            @RequestBody String newUserName,
    ) {
        return userController.updateUser(id, user);
    }

    @DeleteMapping("/deleteUser/{id}")
    public void deleteUser(@PathVariable long id) {

        userController.deleteUserById(id);
    }
}
