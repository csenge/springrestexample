package restExample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import restExample.model.User;
import restExample.service.UserService;

import java.util.List;

@Controller
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    public User insertUser(User user) {
        return userService.insertUser(user);
    }

    public List<User> getAllUsers() {
        return userService.getAllUser();
    }

    public void deleteUserById(long id) {
        userService.deleteUserById(id);
    }

    public User updateUser(long id, User user) {
        return userService.updateUser(id, user);
    }
}
