package restExample.repository;

import org.springframework.stereotype.Component;
import restExample.model.User;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserRepository {

    List<User> userList = new ArrayList<>();

    public List<User> getUserList() {
        return userList;
    }

    public User insertUser(User u) {
        boolean insertionResult = userList.add(u);

        if (insertionResult) {
            return u;
        } else {
            return null;
        }
    }

    public void deleteUserById(long id) {
        for (User u: userList) {
            if (u.getId() == id) {
                userList.remove(u);
                break;
            }
        }
    }

    public User updateUser(long id, User user) {
        for (User u: userList) {
            if (u.getId() == id) {
                u.setName(user.getName());
            }
        }

        return user;
    }

}
