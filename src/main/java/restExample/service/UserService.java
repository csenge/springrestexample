package restExample.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import restExample.model.User;
import restExample.repository.UserRepository;

import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User insertUser(User user) {
        return userRepository.insertUser(user);
    }

    public List<User> getAllUser() {
        return userRepository.getUserList();
    }

    public void deleteUserById(long id) {
        userRepository.deleteUserById(id);
    }

    public User updateUser(long id, User user) {
        return userRepository.updateUser(id, user);
    }
}
