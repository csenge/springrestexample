package restExample.repository;

import org.junit.Test;
import restExample.model.User;

public class UserRepositoryTest {

    UserRepository userRepository = new UserRepository();

    @Test
    public void insertUserTest() {
        //GIVEN
        User userToInsert = new User(1, "testUser");

        //WHEN
        userRepository.insertUser(userToInsert);

        //THEN
        assert (userRepository.userList.size() == 1);

    }
}
